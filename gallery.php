<?php include "assets/view/header.php"; ?>
<?php if (!isset($_GET['group'])) { ?>
    <div style="display:grid;grid-template-columns:1fr 1fr;">
    <div>
        <a href="http://skelbyforsamlingshus.dk/gallery.php?group=inspiration">
            <?php include_once "functions.php";
            imagepicker('inspiration'); ?>
            <h1>Inspiration</h1>
        </a>
    </div>
    <div>
        <a href="http://skelbyforsamlingshus.dk/gallery.php?group=gamlebilleder">
            <?php include_once "functions.php";
            imagepicker('gamlebilleder'); ?>
            <h1>Gamle Billeder</h1>
        </a>
    </div>
<?php }else{
$files = iterator_to_array(new RecursiveDirectoryIterator('assets/img/skelby/' . $_GET['group'], FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS), true); ?>
    <div id="gallery-selection" <?php if (empty($files)) {
        echo "style='display:grid;grid-template-columns:1fr';";
    } ?> >
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js" type="text/javascript"></script>

<link href="https://cdn.jsdelivr.net/npm/nanogallery2@3/dist/css/nanogallery2.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/nanogallery2@3/dist/jquery.nanogallery2.min.js"></script>
    <?php
    $files = iterator_to_array(new RecursiveDirectoryIterator('assets/img/skelby/' . $_GET['group'], FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS), true);
    ksort($files);
    natsort($files);
    if (empty($files)) {
        echo "<h1 style='margin: 0 auto; padding-top:180px;'>Billderne Kommer Snareligst</h1>";
    }

    foreach ($files as $file) {
        // Skip directories to ensure only files are processed
        if ($file->isDir()) {
            continue;
        }


        // Original file path
        $originalPath = $file->getPathname();

        // Directory where thumbnails are stored, relative to the original file's directory or an absolute path
        $thumbnailDir = "assets/img/skelby/" . $_GET['group'] . "/thumbnails";

        // Extract the filename from the original path
        $filename = basename($originalPath);

        // Remove the 'original_' prefix from the filename (if present) and prepend with 'thumbnail_'
        $thumbnailFilename = 'thumbnail_' . $filename;

        // Construct the full path to the thumbnail
        $thumbnailPath = $thumbnailDir . '/' . $thumbnailFilename;

        // Note: You might need to adjust the path transformation logic based on your actual file structure and naming conventions
        echo "<img class='img' src='" . $thumbnailPath . "' data-ngsrc='" . $originalPath . "' data-nanogallery2-lightbox alt='ForsamlingsHust i Skelby'>";
    }
    ?>

<?php }
?>
    <script>
        // Function to update image height based on width
        function updateImageHeight() {
            var gridItems = document.querySelectorAll('.img');
            gridItems.forEach(function (item) {
                var computedStyle = getComputedStyle(item);
                var width = parseFloat(computedStyle.width); // Get computed width as a number
                item.style.height = (width * 3 / 4) + 'px'; // Aspect ratio of 2:1
                var text = item.style.height + ' is half of ' + width;
                var text = item.style.height + ' is half of ' + width;
                console.log(text);
            });
        }

        // Initial height update
        updateImageHeight();

        // Listen for changes to width and update height accordingly
        window.addEventListener('resize', updateImageHeight);

        // You might also want to watch for other changes that could affect width,
        // such as font size changes or content changes, and trigger updateHeight() accordingly.
    </script>
    </div>
<?php include "assets/view/footer.php"; ?>