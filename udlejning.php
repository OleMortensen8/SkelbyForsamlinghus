<?php
include "bootstrap.php";
require './assets/class/Calendar.php';
require './assets/class/Database.php';
require './assets/class/Customer.php';
require './assets/class/Booking.php';
$booking = new Booking();

$bookableCell = new BookableCell($booking);
$bookableCell->routeActions();
include "./assets/view/header.php";
?>
        <main>
           <div id="centerColumn">
                <div style="position:absolute;z-index:-1;left:0; width:100%;max-width:100%; display:flex;flex-direction:row;justify-content:space-between;">
                    <img class="banner" anonymous src="assets/img/signal-2018-09-23-143513.jpg">
                    <img class="banner" src="assets/img/signal-2018-09-23-143531.jpg">
                    <img class="banner" src="assets/img/signal-2018-09-23-143624.jpg">
                </div>

           <h3>Priser for udlejning</h3>
           <h4>reg #: 2650</h4>
           <h4>Konto #:  6403019702</h4>
               <ul style="list-style-type: none;padding:5px; text-align: left;width:17%; margin: 0 auto; border: 10px solid #000;">
                   <li>Depositum - 1000 kr.</li>
           		<li>Hele huset - 2000 kr. - Max 150 personer For både store sal og lille sal</li>
               <li>Små Sale - 1000 kr. - Max 30 personer</li>
               <li>El afregnes ift. dagspris </li>           
               <li>Varmeudgift afregnes ift. dagspris</li>
               <li>Rengøring - 600 kr.</li>
          </ul>
               <?php
           
           $calendar = new Calendar();
            $calendar->setSundayFirst(false); 
            $calendar->attachObserver('showCell', $bookableCell);
            
            echo $calendar->show();

           ?>
           </div>
            <!-- Trigger/Open The Modal -->
<div id="myModal" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <?php $bookableCell->bookingForm(); ?>
    <p id="submission"></p>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.4.js"></script>
<script>
  var modal;
  $(document).ready(function() {
    modal = $("#myModal");
    var btn = $(".open");
    var span = $(".close");
    var sub = $("#sub");
    var sdate = $("#sdate");
    var enddate = $("#enddate");
    var date = $("#date");

    btn.on("click", function() {
        var value = $(this).attr("value");
        sdate.val(value);
        date.html("Fra: " + sdate.val());

        modal.css("display", "block");
    });

    span.on("click", function() {
        modal.css("display", "none");
    });

    sub.on("click", function(event) {
        event.preventDefault();
        submissionMessage();
    });
});

function submissionMessage() {
      var form = $('#form1');
      var navn = $("form[name='form1'] input[name='navnet']").val();
      var adresse = $("form[name='form1'] input[name='adresse']").val();
      var telefon = $("form[name='form1'] input[name='telefon']").val();
      var mail = $("form[name='form1'] input[name='mail']").val();
      form.css("display", "none");
      var submissionElement = $("#submission");
      submissionElement.html("<img style='width:80px;' src='assets/img/Spinner-1s-200px.gif'></img>");
      if (navn === "") {
          submissionElement.html("Navne feltet Skal Udfyldes<br/>");
      }
      if (adresse === "") {
          submissionElement.append("Adresse feltet Skal Udfyldes<br/>");
      }
      if (telefon === "") {
          submissionElement.append("Telefon feltet Skal Udfyldes<br/>");
      }
      if (mail === "") {
          submissionElement.append("Mail feltet Skal Udfyldes<br/>");
      }
      
      if (navn !== "" && adresse !== "" && telefon !== "" && mail !== "") {
        $.ajax({
              type: "POST",
              //url: "<?=$_SERVER['PHP_SELF'];?>",
              url: "udlejning.php?add",
              //url: "assets/class/Booking.php",
              data: form.serialize(),
              datatype: "json",
              success: function (data) {
                  data = JSON.parse(data);
                  if (data.status === "success") {
                    submissionElement.html("Bekræftet: " + data.message);
                  } else {
                      // Handle error response
                      submissionElement.html("Fejl: " + data.message);
                  }
              }, error: function (response) {
                  submissionElement.html(response.responseText);
              }
          });
  }
}

// When the user clicks on <span> (x), close the modal
$('.close').on('click', function() {
    var form = $('#form1');
    modal.css("display", "none");
    $('#submission').html("");
    form.css('display', 'grid');
    window.location.reload();
});

// When the user clicks anywhere outside of the modal, close it
$(window).on('click', function(event) {
    if (event.target == modal[0]) {
        var form = $('#form1');
        modal.css("display", "none");
        $('#submission').html("");
        form.css('display', 'grid');
        window.location.reload();
    }
});
</script>
            <script>
                function handleReloadForEitherParameter(parametersToCheck) {
                    // Parse the URL search string
                    const urlParams = new URLSearchParams(window.location.search);

                    // Check if any of the specified parameters are present
                    let shouldReload = false;
                    parametersToCheck.forEach(param => {
                        if (urlParams.get(param)) {
                            shouldReload = true; // Reload if any one parameter is present
                            urlParams.delete(param); // Remove the parameter from the URL

                            // Save success message in localStorage based on the parameter
                            if (param === "book") {
                                localStorage.setItem('successMessage', 'Booking was successful.');
                            } else if (param === "delete") {
                                localStorage.setItem('successMessage', 'Deletion was successful.');
                            }
                        }
                    });

                    if (shouldReload) {
                        console.log('Reloading the page because one of the specified parameters is present.');

                        // Update the URL without the parameters
                        const newUrl = window.location.pathname + (urlParams.toString() ? '?' + urlParams.toString() : '');
                        window.history.replaceState({}, document.title, newUrl);

                        // Show spinner before reloading
                        const spinnerOverlay = document.createElement('div');
                        spinnerOverlay.id = 'spinner-overlay';
                        spinnerOverlay.innerHTML = `
                <div id="spinner-container">
                    <img src="assets/img/Spinner-1s-200px.gif" alt="Loading..." />
                    <p>Processing your request...</p>
                </div>
            `;
                        document.body.appendChild(spinnerOverlay);

                        // Reload the page after a slight delay to display the spinner clearly
                        setTimeout(() => {
                            window.location.reload();
                        }, 100); // Allows spinner to render fully
                    }
                }

                $(document).ready(function () {
                    // Specify the parameters (check for 'book' OR 'delete')
                    const parametersToCheck = ['book', 'delete'];
                    handleReloadForEitherParameter(parametersToCheck);

                    // Show success message after reload (if any)
                    const successMessage = localStorage.getItem('successMessage');
                    if (successMessage) {
                        const messageContainer = document.createElement('div');
                        messageContainer.id = 'success-message';
                        messageContainer.innerHTML = `
                <div id="message-box">
                    <p>${successMessage}</p>
                    <button id="close-message">OK</button>
                </div>
            `;
                        document.body.appendChild(messageContainer);

                        // Clear the message from localStorage to avoid showing it again
                        localStorage.removeItem('successMessage');

                        // Handle message dismissal
                        $('#close-message').on('click', function () {
                            $('#success-message').fadeOut(function () {
                                $(this).remove();
                            });
                        });
                    }
                });
            </script>
        </main>
<?php include "assets/view/footer.php"; ?>