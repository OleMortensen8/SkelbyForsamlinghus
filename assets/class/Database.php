<?php 
class Database
{
    protected $dbh;
    
    public function __construct()
    {
        try {
            $this->dbh = new PDO(
                sprintf('mysql:host=%s;dbname=%s', getenv('HOST'), getenv('DATABASE')),
                getenv('USERNAME'),
                getenv('PASSWORD')
            );
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
