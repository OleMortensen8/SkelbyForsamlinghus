<?php
                    require "bootstrap.php";

                    class EventManager {

                        public function getArangementer($xml)
                        {
                            if ($xml->children()) {
                                $text = "";
                                foreach ($xml->children() as $arrangement) {
                                    $text .= "<div class='arrangement'>";
                                    $text .= "<h2>" . htmlspecialchars((string) $arrangement->title) . "</h2>";
                                    $text .= "<h3>Dato: " . htmlspecialchars((string)$arrangement->date) . "</h3>";
                                    $text .= "<h3>Tid: " . htmlspecialchars((string)$arrangement->time) . "</h3>";
                                    $text .= "<h3> Stedet: " . htmlspecialchars((string)$arrangement->location) . "</h3>";
                                    $text .= "<h4>Bekrivelse: " . htmlspecialchars((string)$arrangement->description) . "</h4>";
                                    $text .= "</div><hr>";
                                }
                                return $text;
                            } else {
                                return "<h2>Arrangementsliste forberedes... </h2>";
                            }
                        }
                    }