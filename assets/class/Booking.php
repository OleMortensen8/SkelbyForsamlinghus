<?php
class Booking extends Database {
    private $customer = null;

    public function __construct(Customer $customer = null) {
        parent::__construct();
        $this->customer = $customer;
    }
    public function index()
    {
        $statement = $this->dbh->query('SELECT * FROM  Bookings');
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createBooking(array $date, $approved = false) {

        $response = ['status' => 'error', 'message' => 'Unknown error.']; // default response
        try {
            $this->dbh->beginTransaction();
            // Retrieve the current maximum ID
            $maxIdStmt = $this->dbh->query("SELECT MAX(booking_id) FROM Bookings");
            $currentMaxId = $maxIdStmt->fetchColumn();

            $values = array();
            $Date = new DateTime($date[0]);
            for ($i = 0; $i <= $date[1]; $i++) {
                $bookingId = $currentMaxId + 1 + $i; // Calculate the booking ID
                $values[] = array($bookingId, $Date->format('Y-m-d'), $approved, $this->customer->getCustomerId());
                $Date->add(new DateInterval('P1D'));
            }

            // Perform the bulk insertion
            $placeholders = rtrim(str_repeat('(?, ?, ?, ?), ', count($values)), ', ');
            $sql = "INSERT INTO Bookings (booking_id, booking_date, approved, customer_id) VALUES $placeholders";
            $stmt = $this->dbh->prepare($sql);

            // Flatten values array for PDO
            $flattenedValues = array_merge(...$values);
            $stmt->execute($flattenedValues);
            $this->dbh->commit();

            // Calculate the range of new IDs based on the number of days
            $insertedIds = range($currentMaxId + 1, $currentMaxId + 1 + $date[1]);
            $response = ['status' => 'success', 'message' => 'Booking blev Tilføjet Successfuldt.', 'bookingIds' => $insertedIds];
        } catch (Exception $e) {
            // Properly handle exception
            $response = ['status' => 'error', 'message' => $e->getMessage()];
        }
        return json_encode($response);
    }
        
    public function deleteBooking(array $bookingIds) {
        try {
            $idPlaceholders = rtrim(str_repeat('?,', count($bookingIds)), ',');
            $sql = "DELETE FROM Bookings WHERE booking_id IN ($idPlaceholders)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute($bookingIds);
        } catch (Exception $th) {
            echo $th->getMessage();
        }
    }
    public function approveBooking(array $bookingIds){
        try {
            $idPlaceholders = rtrim(str_repeat('?,', count($bookingIds)), ',');
            $sql = "UPDATE Bookings SET approved = 1 WHERE booking_id IN ($idPlaceholders)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute($bookingIds);
        } catch (Exception $th) {
            echo $th->getMessage();
        }
    }
    
 // Other methods related to a booking can be added here.
}
