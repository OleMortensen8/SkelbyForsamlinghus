<?php
class BookableCell
{
    private $booking;
 
    private $currentURL;
     public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->currentURL = htmlentities($_SERVER['REQUEST_URI']);
    }
 
    public function update(Calendar $cal)
    {
        if ($this->isDateBooked($cal->getCurrentDate()) && $this->isDatePending($cal->getCurrentDate())) {
            return $cal->cellContent =
                $this->pendingCell($cal->getCurrentDate());
        }

        if ($this->isDateBooked($cal->getCurrentDate()) && !$this->isDatePending($cal->getCurrentDate())) {
            return $cal->cellContent =
                $this->bookedCell($cal->getCurrentDate());
        }
 
        if (!$this->isDateBooked($cal->getCurrentDate())) {
            return $cal->cellContent =
                $this->openCell($cal->getCurrentDate());
        }
    }
    public function routeActions() {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $response = ['status' => 'error', 'message' => 'Invalid request.']; // default response
        // Handle the AJAX request    
            // ====Add booking====
            if (isset($_POST['add'])) {
                $pendingDay[] = $_POST['startdate'];
                $pendingDay[] = $_POST['enddate'];
                $name = $_POST['navnet'];
                $email = $_POST['mail'];
                $tel = $_POST['telefon'];
                $adresse = $_POST['adresse'];
                $postalCode = $_POST['postnr'];
                $town = $_POST['by'];
                $sal = $_POST['sal'];
                $customer = new Customer($name, $email); // New customer creation
                $bookingObj = new Booking($customer);
                $response = $bookingObj->createBooking($pendingDay, 0);  // capture the booking ID
                
                // Decode the response
                $decodedResponse = json_decode($response, true);
                if ($decodedResponse['status'] === 'success') {
                    // Use PHPMailer to send email with booking IDs
                    $bookingIds = $decodedResponse['bookingIds'];
                    include('phpmailer.php');
                    include('phpmailer_2.php');
                    // You might need to modify phpmailer.php to handle the bookingIds array
                }
                echo $response; // Echo the JSON response
                exit;
            }            

        }
        // ====Delete booking==== 
        if(isset($_GET['delete'])){
            $bookingIds = explode(',', $_GET['ids']);
            $customerId = $this->booking->deleteBooking($bookingIds);
            $customer = new Customer();
            if (!$customer->hasBookings($customerId)) {
                $customer->deleteCustomer($customerId);
            }
        }

        if(isset($_GET['book'])){
          // Split the string of IDs into an array
        $bookingIds = explode(',', $_GET['ids']);
        $this->booking->approveBooking($bookingIds);
        
            }
        
    }
     
    private function openCell($date)
    {
        $today = date('Y-m-d',strtotime('now'));
        if ($date >= $today){
            return '<div class="open" value="'. date('Y-m-d', strtotime($date)) .'">' . date('j',strtotime($date)) . '</div>';
        }else{
            return '<div class="booked" style="background-color:white;">' . date('j', strtotime($date)) . '</div>';
        }
    }

    private function pendingCell($date)
    {
        $today = date('Y-m-d',strtotime('now'));
        if ($date >= $today) {
            return '<div class="pending">' . date('j', strtotime($date)) . '</div>';
        }else{
            return '<div class="pending" style="background-color:white;">' . date('j', strtotime($date)) . '</div>';
        }
    }
  
 
    private function isDatePending($date)
    {
    return in_array($date, $this->pendingDates());
    }

    private function pendingDates()
    {
        return array_map(function ($record) {
            if($record['approved'] == false){
                return $record['booking_date'];
            }
        }, $this->booking->index());
    }
 
    
    private function isDateBooked($date)
    {
        return in_array($date, $this->bookedDates());
    }
 
    private function bookedDates()
    {
        return array_map(function ($record) {
                return $record['booking_date'];
        }, $this->booking->index());
    }
    private function bookedCell($date)
    {
        $today = date('Y-m-d',strtotime('now'));
        if ($date >= $today) {
            return '<div class="booked">' . date('j', strtotime($date)) . '</div>';
        }else{
            return '<div class="booked" style="color:white;">' . date('j', strtotime($date)) . '</div>';
        }
    }
    
    public function bookingForm() {
      echo '<span class="close">x</span><form  id="form1" style="display:block;text-align:left;" method="post" action="">'
            . '<input type="hidden" name="add" />'
            . '<label for="navnet">Navn:</label><input required type="text" id="navnet" name="navnet" placeholder="Navn" />'
            . '<label for="adresse">Adresse:</label><input required type="text" id="adresse" name="adresse" placeholder="Adresse" />'
            . '<div id="form_horizontal"><label for="postnr">Postnr:</label><input style="margin-right:20px;" required type="text" id="postnr" name="postnr" placeholder="Postnr" />'
            . '<label for="by">By:</label><input required type="text" id="by" name="by" placeholder="By" /></div>'
            . '<label for="telefon">Telefon:</label><input required type="tel" id="telefon" name="telefon" placeholder="Telefon" />'
            . '<label for="mail">Email:</label><input type="email" id="mail" name="mail" placeholder="Email"/>'
            . '<label for="sal">Hvilken Sal Ønskes at bookes:</label>'.
            '<div style="margin:0 auto;"><label for="storesal">begge sale:</label><input type="radio" id="storesal" name="sal" value="begge sale" />'
            . '<label for="lillesal"> Lille sal:</label><input type="radio" id="lillesal" name="sal" value="lillesal" /></div>'
            . '<div style="width:100%;display:flex; justify-content:space-between;">'
            . '<input id="sdate" type="hidden" name="startdate" /><label id="date" style="text-align:center;"></label>'
            . '<div><label for="enddate">Periode:</label>'
            . '<select style="margin-left:auto;" id="enddate" type="date" name="enddate">'
            . '<option value="0"> idag</option>'
            . '<option value="1">+1 Dag</option>'
            . '<option value="2">+2 Dage</option>'
            . '<option value="3">+3 Dage</option>'
            . '<option value="4">+4 Dage</option>'
            . '<option value="5">+5 Dage</option>'
            . '<option value="6">+6 Dage</option>'
            . '</select></div>'
            . '</div>'
            . '<input id="sub" class="submit" type="submit" value="Book" />'
            . '</form>';

    }
}