<?php
$name = $_POST['name'] ?? '';
$lastname = $_POST['lastname'] ?? '';
$email = $_POST['mail'] ?? '';
$tel = $_POST['tlf'] ?? '';
$adresse = $_POST['adresse'] ?? '';
$postalCode = $_POST['post'] ?? '';
$town = $_POST['town'] ?? '';

// Validate form data
$errors = [];

if (empty($name)) {
    $errors[] = 'Name is required.';
}

if (empty($lastname)) {
    $errors[] = 'Last name is required.';
}

if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors[] = 'Valid email is required.';
}

if (empty($tel)) {
    $errors[] = 'Telephone number is required.';
}

if (empty($adresse)) {
    $errors[] = 'Address is required.';
}

if (empty($postalCode)) {
    $errors[] = 'Postal code is required.';
}

if (empty($town)) {
    $errors[] = 'Town is required.';
}

if (!empty($errors)) {
    foreach ($errors as $error) {
        echo '<p>' . htmlspecialchars($error) . '</p>';
    }
    exit();
}

$name = $name . ' ' . $lastname;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
// Create a new PHPMailer instance
$mail = new PHPMailer;

// Set up the necessary configuration options
$mail->isSMTP();
$mail->Host = 'websmtp.simply.com';
$mail->SMTPAuth = true;
$mail->Username = 'ue334094@skelby-forsamlingshus.dk';
$mail->Password = 'EnOXiU&O&3sh2jBgZiF5D3&l0FLgn&lkrS%v^jh1OC@gTu@aii0#HXO9690DXhZeSMjowVm30fKs4YN6ITv4ETu8S8AUawbNJ8';
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
$mail->Port = 587;
$mail->CharSet = 'UTF-8';
// Set up the necessary email content
$mail->isHTML(true);
$mail->setFrom('ue334094@skelby-forsamlingshus.dk', 'Kontaktformular');
$mail->addAddress('g.helvig65@gmail.com', 'bestyrelsesNæstFormand');
$mail->addBCC('mette@fiskebaek.com');
$mail->addBCC('olevsmortensen@dbc5radio.dk');
$mail->Subject = "Oplysninger til Medlemsskab";
$mail->Body = ''; // Initialize the body
$mail->Body .= '<br/>Nye medlems Navn: ' . $name;
$mail->Body .= '<br/>Nye medlems Adresse: ' . $adresse . ', ';
$mail->Body .= $postalCode . ', ' . $town;
$mail->Body .= '<br/>Nye medlems Telefon: ' . $tel;
$mail->Body .= '<br/>Nye medlems Mail: ' . $email;
// Send the email
if (!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
header("Location:blivMedlem.php?status=success");
exit();