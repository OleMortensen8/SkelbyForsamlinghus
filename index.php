<?php include 'assets/view/header.php'; ?>
        <main>
        <div id="centerColumn">
            

<div id="twoColumn">
            	<div id="column2">
                <iframe width="100%" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=11.892968416213991%2C54.63258619607408%2C11.910885572433473%2C54.63779608414236&amp;layer=mapnik&amp;marker=54.63519122354269%2C11.90192699432373" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=54.63519&amp;mlon=11.90193#map=17/54.63519/11.90193&amp;layers=N">Vis større kort</a></small>
              </div>    

			  <div class="arrow-up"></div>   
			
			  <div style="width:90%; height:350px;overflow: hidden;">
			  <img style="width:100%; height:100%;margin-left:25px; object-fit: cover;object-position: right;" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/assets/img/skelby/setting3.jpg" alt="" srcset="">
			  </div>
			  <div style="width:73%;text-align:center; margin:auto;margin-left: 66px;">
			<p style="font-size: 16px; line-height: 1.5;"><b>Forsamlingshus i Skelby</b><br>
			<address style="font-style: normal;">Gl. Landevej 66, 4874 Gedser</address>
			<a style="color: blue;" href="tel:+4512345678">+45 22143669</a>
			<!--<<a style="color: blue;" href="mailto:info@skelbyforsamlingshus.dk">info@skelbyforsamlingshus.dk</a>-->
			</p>
		</div>


	</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
  var slideIndex = -1;
  var slideIndex2 = 0;
  var slideIndex3 = 1;
showSlides();
function showSlides() {
if($(slideIndex === $(".img").length)){
  $(".img:eq("+ slideIndex + ")").removeClass('three');
}
if($(slideIndex === $(".img").length-2)){
  $(".img:eq("+ slideIndex + ")").removeClass('three');
}
if($(slideIndex === $(".img").length)){
  $(".img:eq("+ slideIndex + ")").removeClass('two');
}
  $(".img:eq("+ slideIndex + ")").removeClass('one');
  $(".img:eq("+ slideIndex2 + ")").removeClass('two');
  $(".img:eq("+ slideIndex3 + ")").removeClass('three');
  slideIndex++;
  slideIndex2++;
  slideIndex3++;
  console.log(slideIndex,slideIndex2,slideIndex3);
  if (slideIndex === $(".img").length) {
    slideIndex =0;
  }else if(slideIndex === $(".img").length-2){
    $(".img:eq("+ slideIndex + ")").addClass('two');
  }else if(slideIndex === $(".img").length-1){
    $(".img:eq("+ slideIndex + ")").addClass('three');
  }else{
    $(".img:eq("+ slideIndex + ")").addClass('one');
  }


  if (slideIndex2 === $(".img").length) {slideIndex2 = 0; 
    $(".img:eq("+ slideIndex2 + ")").addClass('one');
   }else{
    $(".img:eq("+ slideIndex2 + ")").addClass('two');
  }

  
  if (slideIndex3 === $(".img").length) {slideIndex3 = 0; 
        $(".img:eq("+ slideIndex3 + ")").addClass('one');
  }else  if (slideIndex3 === 1){
    $(".img:eq("+ slideIndex3 + ")").removeClass('one');
        $(".img:eq("+ slideIndex3 + ")").addClass('two');
  }else{
    $(".img:eq("+ slideIndex3 + ")").addClass('three');
  }

  setTimeout(showSlides, 5000); // Change image every 5 seconds
} 
  </script>

        </main>
<?php include 'assets/view/footer.php'; ?>  